# Sujets de projets cours de programmation avancée 2020 Paris8

Dans ce projet, l'objectif est de réaliser une application ou une bibliothèque
en Rust, seul ou jusque à trois étudiant-e-s.

Premièrement vous devrez definir votre sujet, dans un docment `README.md`
au format [markdown](https://docs.gitlab.com/ee/user/markdown.html). Ce document
relativement court doit définir les limites de votre sujet. Ce que vous pensez
réalisisez vous même, quelles denpendances vous utiliseriez.

Et proposera un découpage initial "grossier" du projet avec les grosses étapes
de sa réalisation.


## Dépendances

Vous pouvez utiliser des dépendances il faudra simplement le justifier dans votre `Readme.md`,
attention a produire quand même un travail dont vous pouvez être fier. 😎
## Dates de rendu

Les rendu intermediaires ont pour but de faire gagner des points ou perdre, pour
s'assurer que vous ne rendez pas tout au dernier moment.

- 2020-11-24: Avoir un document qui décrit votre idée (le prof passera relire)
- 2020-11-28: Avoir une idée arrettée par écrit
- 2020-12-15: Rendu intermediaire
- 2020-12-28: Rendu intermediaire
- 2021-01-xx: Rendu du projet
- 2021-01-xx: Celebrate of the project!

### Gestion de projet

Vous devez réaliser des `merges-request` pour intégrer du code dans `master` et
idéalement le faire relire par vos camarades.

Je vous invite a utiliser les issues pour les problèmes ou une solution ne vous
viens pas directement.

### Comment commencer?

Commencer petit, des choses simples, avancer, tester, améliorer, recommencer.

## Sources d'inspiration

Voici des propositions pour vous aides à trouver des idées.

Vous pouvez aussi checker [this-week-in-rust](https://this-week-in-rust.org/),
si vous trouvez un article très intérressant pour vous inspirer pensez à en
faire une mini-synthèse à vos camarades.

### Web

#### Server / Backend

Faire une application web
- [Rocket](https://framagit.org/paris8-rust/projets)
- [Tide.rs](https://github.com/http-rs/tide)

Gestions de database:

- [Diesel.rs](https://diesel.rs/)

Code plus bas level:

- Faire votre mini-libHTTP
- Mini serveur web en utilisant une lib HTTP existante

#### Client / FrontEnd

- Web app avec [yew](https://github.com/yewstack/yew)
- Utiliser WebAssembly pour faire une lib rust pour JavaScript.

### Interfaces graphiques

[Are we gui yet](https://www.areweguiyet.com/)

Faire une interface avec une biblithèque d'interface graphique ou une surcouche
haut level.

#### GTK

- [GTK-rs](https://gtk-rs.org/)
- [relm](https://github.com/antoyo/relm)
- [vgtk](https://github.com/bodil/vgtk)
- [tauri](https://github.com/tauri-apps/tauri)

#### ImGUI

- [imGUI-rs](https://github.com/Gekkio/imgui-rs)

### Jeux

Source d'inspiration [Are We Game Yet](https://arewegameyet.rs/).

- Petit jeu vidéo avec un moteur de jeu vidéo en Rust:
  [amethyst](https://amethyst.rs/), [bevy](https://bevyengine.org/),
- Faire un jeu avec script sur [godot](http://godotengine.org/) + [godot-rust](https://godot-rust.github.io/)
- Bibliothèque de physique 2D

### Langages et compilation

- Mini compilateur d'un langage simple ex: Racket/scheme qui compile vers RiscV
ou WebASM

### Embarqué

[awesome embedded rust](https://github.com/rust-embedded/awesome-embedded-rust)

- Projet bare-metal sur RaspberryPI ou equivalent <- need ref

## systeme

- [sysinfo](https://github.com/GuillaumeGomez/sysinfo) obtenir des info systeme


## Kernel

- Faire un kernel minimal qui affichage un message et gère pourquoi pas le
clavier <- need ref

## Intelligence Artificielle

[Are we learning yet](https://www.arewelearningyet.com/)

- Faire une lib de réseau de neurone, regression linéaire, matrix
- IA+lib: Application a un certain problème en utilisant des lib

## Systemes

- Outil d'analyse des ressources sur un pc <- need ref
- Mini liblxc conteneurisation: limitation droit accès
- Un utilitaire en ligne de commande

